$(document).ready(function(){
  var origHeight = $('.documentView').data('origHeight');

  $('.infoHide').click(function(){                          
    $(".infoHide").hide();
    $(".infoShow").show();
    $(".detailInfo").delay(250).fadeOut(50);
    $(".documentView").animate({
      height:115}, 250);
    $(".documentHeader").animate({
      height:115}, 250);
  });
  $('.infoShow').click(function(){                          
    $(".infoShow").hide();
    $(".infoHide").show();
    $(".detailInfo").delay(250).fadeIn(50);
    $(".documentView").animate({
      height:247}, 250);
    $(".documentHeader").animate({
      height:247}, 250);
  });
});


// Policy Modal Action

$('.policyForms').click(function(){                          
  $(".dialog").fadeIn(500);
});
$('.modal-submit').click(function(){                          
  $(".dialog").fadeOut(500);
});

$('.drawer h2').click(function(){                          
  $(this).toggleClass('open');
  $(this).parent().toggleClass('open');
});



  $('.selectStyle').dropkick({
    change: function (value, label) {
      $(this).dropkick('theme', value);
    }
  });

  $('.dk_container').first().focus();

